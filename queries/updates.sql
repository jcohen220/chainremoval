/**
All the actual update statements needed to remove a chain from a place
 */

--remove chain from place table if chain_id is the same as the Shop
UPDATE place_neustar_test
SET chain_id = null,
last_modified = now()
WHERE chain_id = 1497227817979479882;

--remove chain from chain_sub_persona
delete from chain_sub_persona_test where chain_id = 1497227817979479882;

--remove chain from point_of_interest
UPDATE  point_of_interest_test
SET chain_id = NULL,
last_modified = now()
where chain_id = 1497227817979479882;

-- remove chain from metadata_provider_chain
delete from metadata_provider_chain where chain_id = 1497227817979479882;

-- remove chain from metadata_provider_chain
delete from profeed_chain_test where chain_id = 1497227817979479882;

--remove chain from chain table if ID is the Shop
delete from chain_test where id = 1497227817979479882;
