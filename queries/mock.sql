/**
 Mock the tables we need to edit to remove a chain fully
 */

-- Mock chain ddl
create table if not exists chain_test
(
	id bigint default id_generator() not null
		constraint chain_pkey_test
			primary key,
	name varchar(100) not null,
	patterns character varying[],
	neustar_chain_id integer
		constraint unique_neustar_chain_id_test
			unique,
	created_on timestamp default now()
);

alter table chain_test owner to "nktProdMaster";

create index if not exists ix_chain_name_test
	on chain_test (name);

-- Mock chain insert sample data
 insert into chain_test
select * from chain where name in ('The Shop', 'Bikram Yoga', 'Denver Mattress')

-- Mock place_neustar ddl
create table if not exists place_neustar_test
(
	id bigint default id_generator() not null
		constraint place_neustar_test_pkey
			primary key,
	lat double precision not null,
	lon double precision not null,
	chain_id bigint,
	created_on timestamp default now(),
	name varchar(200) not null,
	path bigint[],
	neustar_pid bigint
		constraint unique_neustar_test_pid
			unique,
	categoryid bigint,
	distance integer,
	geofence geometry not null
		constraint enforce_valid_geofence_test
			check (st_isvalid(geofence)),
	manual_geofence bit,
	master_id bigint,
	socialradar_id bigint
		constraint unique_socialradar_id_test
			unique,
	is_enabled bit,
	last_modified timestamp default now() not null,
	constraint neustar_test_socialradar
		check ((neustar_pid IS NOT NULL) OR (socialradar_id IS NOT NULL))
);

alter table place_neustar_test owner to "nktProdMaster";

create index if not exists ix_place_neustar_test_chain_id
	on place_neustar_test (chain_id);

create index if not exists ix_place_neustar_test_categoryid
	on place_neustar_test (categoryid);

create index if not exists ix_place_neustar_test_geofence
	on place_neustar_test (geofence);

create index if not exists ix_place_neustar_test_location
	on place_neustar_test (st_point(lon, lat));

create index if not exists ix_place_neustar_test_geofence_geog
	on place_neustar_test ((geofence::geography));

--Mock place_neustar sample data
INSERT INTO place_neustar_test
WITH shop as (
  SELECT id
  FROM chain
  WHERE chain.name in ('The Shop', 'Bikram Yoga', 'Denver Mattress'))
SELECT place.*
FROM place_neustar place, shop
WHERE place.chain_id = shop.id;

--Mock chain_sub_persona ddl
create table if not exists chain_sub_persona_test
(
	chain_id bigint not null
		constraint chain_sub_persona_chain_id_fkey_test
			references chain,
	sub_persona_id bigint not null
		constraint chain_sub_persona_sub_persona_id_fkey_test
			references "SubPersona",
	constraint chain_sub_persona_pkey_test
		primary key (chain_id, sub_persona_id)
);

alter table chain_sub_persona_test owner to "nktProdMaster";

--Mock chain_sub_persona insert
insert into chain_sub_persona_test
with shop as (
  select id
  from chain
  where name in ('The Shop', 'Bikram Yoga', 'Denver Mattress'))
  select chain_sub_persona.*
from chain_sub_persona, shop
  where chain_sub_persona.chain_id = shop.id;

-- Mock point_of_interest ddl
create table if not exists point_of_interest_test
(
	id bigserial not null
		constraint point_of_interest_pkey_test
			primary key,
	polygon_id bigint not null,
	metadata_provider_id smallint not null
		constraint point_of_interest_metadata_provider_id_fkey_test
			references metadata_provider,
	external_key varchar(50) not null,
	chain_id bigint
		constraint point_of_interest_chain_id_fkey_test
			references chain,
	org_id bigint
		constraint point_of_interest_org_id_fkey_test
			references auth."Organization",
	confidence smallint,
	is_enabled bit default (1)::bit(1) not null,
	created_on timestamp default now() not null,
	last_modified timestamp default now() not null
);

alter table point_of_interest_test owner to "nktProdMaster";

create index if not exists point_of_interest_polygon_id_test
	on point_of_interest_test (polygon_id);

create index if not exists ix_point_of_interest_provider_key_test
	on point_of_interest_test (metadata_provider_id, external_key);

create index if not exists ix_point_of_interest_polygon_test
	on point_of_interest_test (polygon_id);

create index if not exists ix_point_of_interest_chain_test
	on point_of_interest_test (chain_id);

-- Mock Point of interest insert sample data
insert into point_of_interest_test
with shop as (
  select id
  from chain
  where name in ('The Shop', 'Bikram Yoga', 'Denver Mattress'))
  select point_of_interest.*
	from point_of_interest, shop
  where point_of_interest.chain_id = shop.id;

--Mock AudienceGroupChain ddl
create table if not exists auth."AudienceGroupChainTest"
(
	group_id bigint not null
		constraint "AudienceGroupChain_group_id_fkey_test"
			references auth."AudienceGroup",
	persona_id bigint,
	sub_persona_id bigint,
	chain_id bigint not null,
	states char(2) [],
	constraint "AudienceGroupChain_unique_test"
		unique (persona_id, sub_persona_id, group_id, chain_id)
);

alter table auth."AudienceGroupChainTest" owner to "nktProAuthMaster";

--Insert sample data into AudienceGroupChainTest

--Mock metadata_provider_chain
create table if not exists metadata_provider_chain_test
(
	metadata_provider_id smallint not null
		constraint metadata_provider_chain_metadata_provider_id_fkey_test
			references metadata_provider,
	chain_id bigint not null
		constraint metadata_provider_chain_chain_id_fkey_test
			references chain,
	external_key varchar(50) not null,
	created_on timestamp default now() not null,
	last_modified timestamp default now() not null,
	is_enabled boolean default true not null,
	constraint metadata_provider_chain_pk_test
		primary key (metadata_provider_id, chain_id)
);

alter table metadata_provider_chain_test owner to "nktProdMaster";

--Insert sample data into metadata_provider_chain
insert into metadata_provider_chain_test
with shop as (
  select id
  from chain
  where name in ('The Shop', 'Bikram Yoga', 'Denver Mattress'))
  select metadata_provider_chain.*
	from metadata_provider_chain, shop
  where metadata_provider_chain.chain_id = shop.id;

-- Mock prodfeed_chain ddl
create table if not exists prodfeed_chain_test
(
	job_name varchar(100) not null,
	aud_id bigint not null
		constraint aud_id_fkey_test
			references branded_audience,
	chain_id bigint not null
		constraint chain_id_fkey_test
			references chain,
	percent_rank double precision not null,
	cumulative_7_day_device_count bigint not null
);

alter table prodfeed_chain_test owner to "nktProdMaster";

--insert sample data into prodfeed_chain


