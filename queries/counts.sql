/**
Get counts of places associated with The Shop chain
 */
with shop as (
  select id
  from chain
  where name = 'The Shop')
select place.name, count(distinct(place.id))
from place_neustar place, shop
where place.chain_id = shop.id
group by place.name
order by count(distinct(place.id)) desc;

--Count audiences with the chain (should be 0)
with shop as (
  select id
  from chain
  where name in ('The Shop', 'Bikram Yoga', 'Denver Mattress'))
  select auth."AudienceGroupChainTest".*
	from auth."AudienceGroupChainTest", shop
  where auth."AudienceGroupChainTest".chain_id = shop.id;