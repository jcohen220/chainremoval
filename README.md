# Chain Removal
This repo houses SQL code for finding all `places` associated with a particular `chain` and update statements needed 
to disassociate the two, essentially "removing" the chain.

The goal is that this can serve as a POC/prototype for a future place/chain API or tooling to make updates of this nature less manual.

### Background
`The Shop` is a chain in Reveal's system that is not actually a chain, the
individual places that comprise this chain are unaffiliated with eachother
and thus the chain needs to be "Removed" AKA the places need
their chain_ids set to `null`.

### Queries

# Initial discovery
* `counts.sql`
# Update queries
* `updates.sql` 
# Sandbox table ddls/inserts 
* `mock.sql`

